import random;

VIDE = ' '
ROND = 'O'
CROIX = 'X'

plateau = [[VIDE,VIDE,VIDE],\
           [VIDE,VIDE,VIDE],\
           [VIDE,VIDE,VIDE]]


def joueur_humain():
    x = int(input("x ? "))
    while (x < 0 or x > 2):
        x = int(input("invalid. x ? "))
    y = int(input("y ? "))
    while (y < 0 or y > 2):
        y = int(input("invalid. y ? "))
    if (plateau[x][y] != VIDE):
        print("non empty position.")
        return joueur_humain()
    return x, y


def joueur_ia():
    x = random.randint(0,2)
    y = random.randint(0,2)
    while (plateau[x][y] != VIDE):
        x = random.randint(0,2)
        y = random.randint(0,2)
    return x, y


def egalite():
    for row in plateau:
        for box in row:
            if box == VIDE:
                return False
    return True

def victoire(symbol):
    for i in range(0,3):
        if ((plateau[i][0] == symbol and plateau[i][1] == symbol and plateau[i][2] == symbol) or \
            (plateau[0][i] == symbol and plateau[1][i] == symbol and plateau[2][i] == symbol)):
            return True
    if ((plateau[0][0] == symbol and plateau[1][1] == symbol and plateau[2][2] == symbol) or\
        (plateau[0][2] == symbol and plateau[1][1] == symbol and plateau[2][0] == symbol)):
        return True
    return False


def afficher():
    for x in range(0,3):
        for y in range(0,3):
            print(plateau[x][y], ('\n', '|')[y<2], end="", sep="")
        if (x != 2):
            print("-+-+-")


player = [
    ("humain", ROND, joueur_humain),
    ("IA", CROIX, joueur_ia)
]
current_player = 0

afficher()
while True:
    pos = player[current_player][2]()
    plateau[pos[0]][pos[1]] = player[current_player][1]
    print('joueur ', player[current_player][0], " joue à la position ", pos)
    afficher()
    if (victoire(player[current_player][1])):
        print("joueur ", player[current_player][0], " gagne la partie !")
        break
    elif (egalite()):
        print("égalite !")
        break
    current_player ^= 1
